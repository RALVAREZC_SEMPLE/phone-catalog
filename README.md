```
 ___ __ __      ________       ______         ___ __ __      ______       __   __      ________      __          
/__//_//_/\    /_______/\     /_____/\       /__//_//_/\    /_____/\     /_/\ /_/\    /_______/\    /_/\         
\::\| \| \ \   \::: _  \ \    \::::_\/_      \::\| \| \ \   \:::_ \ \    \:\ \\ \ \   \__.::._\/    \:\ \        
 \:.      \ \   \::(_)  \ \    \:\/___/\      \:.      \ \   \:\ \ \ \    \:\ \\ \ \     \::\ \      \:\ \       
  \:.\-/\  \ \   \:: __  \ \    \_::._\:\      \:.\-/\  \ \   \:\ \ \ \    \:\_/.:\ \    _\::\ \__    \:\ \____  
   \. \  \  \ \   \:.\ \  \ \     /____\:\      \. \  \  \ \   \:\_\ \ \    \ ..::/ /   /__\::\__/\    \:\/___/\
    \__\/ \__\/    \__\/\__\/     \_____\/       \__\/ \__\/    \_____\/     \___/_(    \________\/     \_____\/
```

# Microservice Phone Catalog

## Introduction

Microsercicio that expose three endpoins for the selection of terminals by the client.

These End Points are the following:

* `/catalog` --> 	Returns the list of terminals in the database.

```
 http://localhost:8080/catalog
 Response:
 {
   "listResponse": [
     {
       "id": 1,
       "name": "phone1",
       "imgRef": "/images/phone1.jpg",
       "characteristics": null,
       "amount": 101,
       "currency": "€"
     },
     {
       "id": 5,
       "name": "phone1",
       "imgRef": "/images/phone5.jpg",
       "characteristics": null,
       "amount": 105,
       "currency": "€"
     },
     {
       "id": 6,
       "name": "phone1",
       "imgRef": "/images/phone6.jpg",
       "characteristics": null,
       "amount": 106,
       "currency": "€"
     },
     {
       "id": 7,
       "name": "phone1",
       "imgRef": "/images/phone7.jpg",
       "characteristics": null,
       "amount": 107,
       "currency": "€"
     },
     {
       "id": 8,
       "name": "phone1",
       "imgRef": "/images/phone8.jpg",
       "characteristics": null,
       "amount": 108,
       "currency": "€"
     },
     {
       "id": 9,
       "name": "phone1",
       "imgRef": "/images/phone9.jpg",
       "characteristics": null,
       "amount": 109,
       "currency": "€"
     },
     {
       "id": 10,
       "name": "phone1",
       "imgRef": "/images/phone10.jpg",
       "characteristics": null,
       "amount": 110,
       "currency": "€"
     },
     {
       "id": 2,
       "name": "phone2",
       "imgRef": "/images/phone2.jpg",
       "characteristics": null,
       "amount": 102,
       "currency": "€"
     },
     {
       "id": 3,
       "name": "phone3",
       "imgRef": "/images/phone3.jpg",
       "characteristics": null,
       "amount": 103,
       "currency": "€"
     },
     {
       "id": 4,
       "name": "phone4",
       "imgRef": "/images/phone4.jpg",
       "characteristics": null,
       "amount": 104,
       "currency": "€"
     }
   ]
 }

```

* `/phone/{id}` --> Return a phone selected by id.

```
http://localhost:8080/phone/2
 Response: {"id":2,"name":"phone2","imgRef":"/images/phone2.jpg","characteristics":null,"amount":102,"currency":"€"}
```

* `/order` --> Write in console the total amount of the selected terminals and the Client that has selected them, also return the ordet and the total amount.

```
http://localhost:8080/order
 {
  "client": {
    "id": 1,
    "name": "Pepito",
    "surname": "Pere",
    "email": "pperez@gmail.com"
  },
  "lphones": [
    {
      "id": 1
    },
    {
      "id": 2
    },
    {
      "id": 3
    }
  ]
}
Response :
{
"order":{
"client":{"id": 1, "name": "Pepito", "surname": "Pere", "email": "pperez@gmail.com"…},
"phones":[
{"id": 1, "name": null, "imgRef": null, "amount": 0, "currency": null…},
{"id": 2, "name": null, "imgRef": null, "amount": 0, "currency": null…},
{"id": 3, "name": null, "imgRef": null, "amount": 0, "currency": null…}
]
},
"totalOrder": "306€"
}

```
* Output console

```
 Ordrer from Client: Pepito Pere
 Phone List:
 Select phone by id 1
 1 phone1 101€
 Select phone by id 2
 2 phone2 102€
 Select phone by id 3
 3 phone3 103€
 Total....306€
```

## Installation:
The `.sql` file (src/main/resources/db/semple.sql) is attached to generate the table and the sample data in the database.

The file `docker-compose.yml` has been added for the generation of the image with the environment variables in docker.

## Propertie file:

For this example the properties have been added:

```
spring.datasource.url=${BBDD_URL:jdbc:postgresql://localhost:5432/CATALOG}
spring.datasource.username=${BBDD_USER:postgres}
spring.datasource.password=${BBDD_PASSWORD:password}
spring.datasource.driver-class-name=${BBDD_DRIVER:org.postgresql.Driver}
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
```


## How would you improve the system?

* It could be improved by adding security for the connection with the microservice, validations of the fields, client management that makes the order and recording the result of the order in the database.

* I could also add `swagger` to generate documentation.

* I have added circuit breaker `@HystrixCommand` to manage possible connection errors with the telephone validation endpoint.

## How would you avoid your order API to be overflow?

I could manage the autoscaling in docker to increase the microservice stability and that a Load Balancer  
or the platform itself `(kubernetes or openshift)`  manages the load and a `Api Gateway` to stop the excess connections.
