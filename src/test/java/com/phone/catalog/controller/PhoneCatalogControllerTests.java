package com.phone.catalog.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.phone.catalog.bean.Phone;
import com.phone.catalog.service.IPhoneService;

// TODO: Auto-generated Javadoc
/**
 * The Class PhoneCatalogControllerTests.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(PhoneCatalogController.class)
public class PhoneCatalogControllerTests {

    
    /** The mvc. */
    @Autowired
    private MockMvc mvc;

    /** The service. */
    @MockBean
    private IPhoneService service;
    
	/**
	 * Context loads.
	 */
	public void contextLoads() {
	}
	
	
	/**
	 * Gets the catalog.
	 *
	 * @return the catalog
	 * @throws Exception the exception
	 */
	@Test
    public void getCatalog() throws Exception {
	    
	    
	    mvc.perform(get("/catalog")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andExpect(status().isOk());
    }
    
	/**
	 * Gets the phone.
	 *
	 * @return the phone
	 * @throws Exception the exception
	 */
	@Test
    public void getPhone() throws Exception {
	
	    Integer id = new Integer(2);
	    when(service.findById(id)).thenReturn(Optional.of(new Phone()));        
        mvc.perform(get("/phone/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
	

}
