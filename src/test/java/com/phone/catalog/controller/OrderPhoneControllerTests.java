package com.phone.catalog.controller;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.phone.catalog.bean.Client;
import com.phone.catalog.bean.Order;
import com.phone.catalog.bean.Phone;
import com.phone.catalog.service.IOrderService;
import com.phone.catalog.service.IPhoneService;

// TODO: Auto-generated Javadoc
/**
 * The Class OrderPhoneControllerTests.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(OrderPhoneController.class)
public class OrderPhoneControllerTests {
  
    /** The service. */
    @MockBean
    private IPhoneService service;
    
    /** The order. */
    @MockBean
    private IOrderService order;
    
  
    /** The mvc. */
    @Autowired
    private MockMvc mvc;
    
    
    /**
     * Order.
     *
     * @throws Exception the exception
     */
    @Test
    public void order() throws Exception {
        Order order = new Order();
        
        Client client = new Client();
        client.setId(1);
        client.setName("Perico");
        client.setSurname("Perez");
        client.setEmail("pperez@gmi.com");
        order.setClient(client);
        
        List<Phone> lPhone = new ArrayList<>();
        Phone phone1 = new Phone();
        phone1.setId(1);
        lPhone.add(phone1);

        Phone phone2 = new Phone();
        phone2.setId(2);
        lPhone.add(phone2);        
        order.setPhones(lPhone);
              
      
        mvc.perform(post("/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(order)))
                .andExpect(status().isOk());
    }
    
    /**
     * As json string.
     *
     * @param obj the obj
     * @return the string
     */
    /*
     * converts a Java object into JSON representation
     */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
