package com.phone.catalog.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.phone.catalog.bean.Phone;

// TODO: Auto-generated Javadoc
/**
 * The Class PhoneServiceTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoneServiceTest {

    /** The service. */
    @Autowired
    private IPhoneService service;
    
    /**
     * Gets the phone catalog.
     *
     * @return the phone catalog
     * @throws Exception the exception
     */
    @Test
    public void getPhoneCatalog() throws Exception  {
      
        Iterable<Phone> lphone = service.findAll(); 
        assertNotNull(lphone);
        assertNotNull(lphone.iterator().hasNext());
    }
    
    /**
     * Gets the phone by id.
     *
     * @return the phone by id
     * @throws Exception the exception
     */
    @Test
    public void getPhoneById() throws Exception  {
       
        Integer id= new Integer(2);
        Optional<Phone> phone = service.findById(id); 
        assertNotNull(phone);
        assertEquals(phone.get().getId(), id);
    }
    
    
    /**
     * Gets the total amount.
     *
     * @return the total amount
     * @throws Exception the exception
     */
    @Test
    public void getTotalAmount() throws Exception  {
       
        Integer ids[] = { 1, 2};
        
       
        Iterable<Phone> lphone = service.findAllById(Arrays.asList(ids));
        long  total = 0;
        for(Phone phone: lphone) {
            total += phone.getAmount();
        }
        assertNotNull(total);
        assertEquals(total, 203L);
    }
}
