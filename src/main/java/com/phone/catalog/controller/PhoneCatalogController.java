package com.phone.catalog.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.phone.catalog.bean.Phone;
import com.phone.catalog.bean.ResponseListPhone;
import com.phone.catalog.service.IPhoneService;
import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/** The Constant log. */
@Slf4j
@RestController
public class PhoneCatalogController extends BaseController {
    
    /** The service. */
    @Autowired
    private IPhoneService phoneService;
    
    /**
     * Gets the catalog.
     *
     * @return the catalog
     */
    @GetMapping(path = "/catalog")
    public ResponseEntity<ResponseListPhone> getCatalog() {
        log.info("Return phone catalog"); 
       
        ResponseListPhone response =  new ResponseListPhone();
        response.setListResponse(phoneService.findAll());
        return ResponseEntity.ok(response);
    }
    
    
    /**
     * Gets the phone.
     *
     * @param id the id
     * @return the phone
     */
    @GetMapping("/phone/{id}")
    public ResponseEntity<Phone> getPhone(@PathVariable("id") Integer id) {
       log.info("Select phone by id {}",id); 
       Optional<Phone> phone = phoneService.findById(id);
       return ResponseEntity.ok(phone.get());
    }
    
}
