package com.phone.catalog.controller;

import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.phone.catalog.bean.Order;
import com.phone.catalog.bean.Phone;
import com.phone.catalog.bean.ResponseOrder;
import com.phone.catalog.service.IOrderService;
import com.phone.catalog.service.IPhoneService;
import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/** The Constant log. */
@Slf4j
@RestController
public class OrderPhoneController extends BaseController {

    /** The Constant CURRENCY. */
    private static final String CURRENCY= "€";
    
    /** The service. */
    @Autowired
    private IPhoneService service;
    
    /** The order service. */
    @Autowired
    private IOrderService orderService;
    
    /**
     * Post order.
     *
     * @param request the request
     * @return the response entity
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
    @PostMapping(path = "/order", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResponseOrder> postOrder(@RequestBody Order request) {
        log.info("Ordrer from Client: {} {}", request.getClient().getName(), request.getClient().getSurname());
        log.info("Phone List:");
        ResponseOrder resOrder = new ResponseOrder();
        resOrder.setOrder(request);
        if(request.getPhones() ==null || request.getPhones().isEmpty()) {
            log.error("Phone List is null or empty.");
            resOrder.setTotalOrder("0" + CURRENCY );
            return ResponseEntity.ok(resOrder);
        }
        
        List<Integer> ids =  orderService.validatePhones(request.getPhones());
            
        
        Iterable<Phone> Phones = service.findAllById(ids);
        
        LongSummaryStatistics total = StreamSupport.stream(Phones.spliterator(), false)
                                        .collect(Collectors.summarizingLong(Phone::getAmount));
        
       
               
      
        resOrder.setTotalOrder(total.getSum() + CURRENCY );
        
        log.info("Total....{} ", resOrder.getTotalOrder() );
        
        return ResponseEntity.ok(resOrder);
    }
    
    
}
