package com.phone.catalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;

// TODO: Auto-generated Javadoc
/**
 * The Class PhoneCatalogApplication.
 */
@SpringBootApplication
@EnableCircuitBreaker
public class PhoneCatalogApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(PhoneCatalogApplication.class, args);
	}
}
