package com.phone.catalog.bean;

import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new client.
 */
@Data
public class Client {
    
    /** The id. */
    private int id;
    
    /** The name. */
    private String name;
    
    /** The surname. */
    private String surname;
    
    /** The email. */
    private String email;
}
