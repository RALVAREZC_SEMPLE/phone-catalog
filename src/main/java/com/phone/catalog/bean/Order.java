package com.phone.catalog.bean;

import java.util.List;
import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new order.
 */
@Data
public class Order {
    
    /** The client. */
    private Client client;
    
    /** The phones. */
    private List<Phone> phones;
}
