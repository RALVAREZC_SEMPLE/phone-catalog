package com.phone.catalog.bean;

import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new response order.
 */
@Data
public class ResponseOrder {
    
    /** The order. */
    public Order order;
    
    /** The total order. */
    public String totalOrder;    
}
