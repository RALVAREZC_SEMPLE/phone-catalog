package com.phone.catalog.bean;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new phone.
 */
@Data
@Entity
@Table(name = "PHONE", schema="CATALOG")
public class Phone {
    
    /** The id. */
    @Id
    private Integer id;
    
    /** The name. */
    private String name;
    
    /** The img ref. */
    @Column(name = "imgref")
    private String imgRef;
    
    /** The amount. */
    private long amount;
    
    /** The currency. */
    private String currency;
    
}
