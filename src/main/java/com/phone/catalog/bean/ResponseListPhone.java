package com.phone.catalog.bean;

import java.util.List;
import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new response list phone.
 */
@Data
public class ResponseListPhone {
    
    /** The list response. */
    private Iterable<Phone> listResponse;
}
