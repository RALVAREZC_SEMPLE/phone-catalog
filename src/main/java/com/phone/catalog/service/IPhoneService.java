package com.phone.catalog.service;

import org.springframework.data.repository.CrudRepository;
import com.phone.catalog.bean.Phone;

/**
 * The Interface IPhoneService.
 */
public interface IPhoneService extends CrudRepository<Phone, Integer> {
}
