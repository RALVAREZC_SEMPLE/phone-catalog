package com.phone.catalog.service;

import java.util.List;
import com.phone.catalog.bean.Phone;

// TODO: Auto-generated Javadoc
/**
 * The Interface IOrderService.
 */
public interface IOrderService {
    
    /**
     * Validate phones.
     *
     * @param list the list
     * @return the list
     */
    public List<Integer> validatePhones(List<Phone> list);
}
