package com.phone.catalog.service;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.phone.catalog.bean.Phone;
import lombok.extern.slf4j.Slf4j;

// TODO: Auto-generated Javadoc
/** The Constant log. */
@Slf4j
@Service
public class OrderServiceImpl implements IOrderService {
    
    
    /** The Constant URL_PHONE. */
    private static final String URL_PHONE = "http://localhost:8080/phone/";
    private RestTemplate restTemplate = new RestTemplate();
    /* (non-Javadoc)
     * @see com.phone.catalog.service.IOrderService#validatePhones(java.util.List)
     */
    @Override
    public List<Integer> validatePhones(List<Phone> list) {
        
        return list.stream().filter(phone -> exist(phone) ).map(Phone::getId).collect(Collectors.toList());
        
    }
    
    /**
     * Exist.
     *
     * @param phone the phone
     * @return true, if successful
     */
    @HystrixCommand(fallbackMethod = "existfallbackMethod")
    private boolean exist(Phone phone) {
        
        
        ResponseEntity<Phone> response = restTemplate.getForEntity(URL_PHONE+phone.getId(),Phone.class);
        if(response.getBody() == null) {
            log.error("Phone {} does´t exist.",phone.getId());
            return false;
        }else {
            log.info("{} {} {}{} ",response.getBody().getId(), response.getBody().getName(),response.getBody().getAmount(),response.getBody().getCurrency());
            return true;
        }
    }
    
    /**
     * Existfallback method.
     *
     * @param phone the phone
     * @return true, if successful
     * @throws Throwable the throwable
     */
    public boolean existfallbackMethod(Phone phone) throws Throwable {
        log.info("the end point {} isn´t started ",URL_PHONE);
        return false;
    }
}
