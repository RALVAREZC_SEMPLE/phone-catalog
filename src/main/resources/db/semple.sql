DROP SCHEMA IF EXISTS CATALOG CASCADE;

CREATE SCHEMA IF NOT EXISTS CATALOG;

CREATE TABLE IF NOT EXISTS CATALOG.PHONE (
	id          SERIAL PRIMARY KEY,
	name        VARCHAR(256)  NOT NULL,
	imgRef      VARCHAR(128)  NOT NULL,
	amount      VARCHAR(256)  NOT NULL,
	currency    VARCHAR(1024) NOT NULL
);


INSERT INTO CATALOG.PHONE (id, name, imgRef, amount, currency) VALUES
(1 ,'phone1', '/images/phone1.jpg', '101','€'),
(2 ,'phone2', '/images/phone2.jpg', '102','€'),
(3 ,'phone3', '/images/phone3.jpg', '103','€'),
(4 ,'phone4', '/images/phone4.jpg', '104','€'),
(5 ,'phone5', '/images/phone5.jpg', '105','€'),
(6 ,'phone6', '/images/phone6.jpg', '106','€'),
(7 ,'phone7', '/images/phone7.jpg', '107','€'),
(8 ,'phone8', '/images/phone8.jpg', '108','€'),
(9 ,'phone9', '/images/phone9.jpg', '109','€'),
(10 ,'phone10', '/images/phone10.jpg', '110','€')
;


